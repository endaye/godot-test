# Version
# @see Hidden Moss: https://hiddenmoss.com/
# @see Miss. Peacock: https://miss-peacock.com/
# @author Yuancheng Zhang

extends Node

# Version
const MAJOR = 0
const MINOR = 4
const BUGFIX = 2
const COMMIT = 492335
const VERSION = "v%s.%s.%s.%s" % [MAJOR, MINOR, BUGFIX, COMMIT]
