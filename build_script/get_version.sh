#!/bin/sh

# 需要操作的脚本路径
FILE=./script/manager/mgr_version.gd
VERSION="0.0.0.0"
if [ -f $FILE ]; then
    # 版本号变量
    major=''
    minor=''
    bugfix=''
    commit=''
    arr=()
    # 读取脚本内容
    while read line || [[ -n ${line} ]]; do
        arr[${#arr[*]}]="$line"
    done <$FILE
    # 重写脚本内容
    for ((i = 1; i < ${#arr[*]}; i++)); do
        str=${arr[i]:0:8}
        if [ "$str" == "const MA" ]; then
            # 读取major版本号
            major=$(echo ${arr[i]} | tr -cd "[:digit:]")
        elif [ "$str" == "const MI" ]; then
            # 读取minor版本号
            minor=$(echo ${arr[i]} | tr -cd "[:digit:]")
        elif [ "$str" == "const BU" ]; then
            # 读取bugfix版本号
            bugfix=$(echo ${arr[i]} | tr -cd "[:digit:]")
        elif [ "$str" == "const CO" ]; then
            # 将commit版本号设为总commit次数
            commit=$(echo ${arr[i]} | tr -cd "[:digit:]")
        fi
    done

    # 当前版本号
    VERSION=$major"."$minor"."$bugfix"."$commit
fi
echo $VERSION